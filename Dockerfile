# syntax=docker/dockerfile:experimental
from rustlang/rust:nightly
ENV HOME=/usr/src
WORKDIR $HOME/app

COPY . $HOME/app/

RUN cargo install cargo-watch cargo-tarpaulin cargo-edit

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/usr/src/app/target

CMD tail -f /dev/null
