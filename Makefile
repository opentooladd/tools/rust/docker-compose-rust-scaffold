DOCKER_BUILDKIT=1

start:
	DOCKER_BUILDKIT=$(DOCKER_BUILDKIT) docker-compose up

# attach to the rust container
attach:
	docker-compose run rust bash