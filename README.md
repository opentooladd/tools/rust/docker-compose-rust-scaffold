# <replace: your project name>

A docker-compose scaffolding for rust development

## Docker BuildKit

Docker BuildKit must be enabled.
It is handled in the Makefile.

see [Docker Buildkit](https://docs.docker.com/develop/develop-images/build_enhancements/#to-enable-buildkit-builds)